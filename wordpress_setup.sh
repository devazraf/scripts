#!/bin/bash
#
# copy from https://gist.github.com/bgallagh3r/5cf73cf92303ebff4799

# Installation script for a Wordpress 3.0 website on Ubuntu 10.04
#
# Josh Kersey
# Created: May 15, 2012
# Last Update: June 13, 2012

# get setup parameters
echo "apache vhost name (one word):"
read vhost_name
echo "site url (ex: mysite.cabedgedev.com):"
read site_url
echo "ftp username:"
read ftpuser
echo "mysql root password:"
read mysqlrootpass
echo "wordpress database name:"
read dbname
echo "wordpress database username:"
read dbuser

# Generate random passwords
ftppass=`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 10`
dbpass=`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 10`

# create the FTP user
sudo useradd -d /var/www/$vhost_name/ -s /usr/sbin/nologin $ftpuser

# Update the new users password
sudo usermod -p `echo $ftppass | openssl passwd -1 -stdin` $ftpuser

# create database and user for database
Q1="create database if not exists $dbname;"
Q2="grant all privileges on $dbname.* to $dbuser@'localhost' identified by '$dbpass';"
Q3="flush privileges;"
SQL="${Q1}${Q2}${Q3}"

sudo mysql -uroot -p$mysqlrootpass -e"$SQL"

# download Wordpress
sudo wget -O -n /usr/local/src/wordpress.tar.gz https://wordpress.org/latest.tar.gz
sudo tar -C /var/www/ -xvzf /usr/local/src/wordpress.tar.gz
sudo mv /var/www/wordpress /var/www/$vhost_name

# configure Wordpress
sudo cp /var/www/$vhost_name/wp-config-sample.php /var/www/$vhost_name/wp-config.php
sudo wget -O /usr/local/src/wp.keys https://api.wordpress.org/secret-key/1.1/salt/
sudo sed -i "s/database_name_here/$dbname/" /var/www/$vhost_name/wp-config.php
sudo sed -i "s/username_here/$dbuser/" /var/www/$vhost_name/wp-config.php
sudo sed -i "s/password_here/$dbpass/" /var/www/$vhost_name/wp-config.php
sudo sed -i '/#@-/r /usr/local/src/wp.keys' /var/www/$vhost_name/wp-config.php
sudo sed -i "/#@+/,/#@-/d" /var/www/$vhost_name/wp-config.php

# configure vhost
sudo bash -c "cat > /etc/apache2/sites-available/$vhost_name" <<EOF
<VirtualHost *:80>
        ServerAdmin developers@cabedge.com
        ServerName $site_url

        DocumentRoot /var/www/$vhost_name/
        <Directory />
                Options None
                AllowOverride None
                Order deny,allow
                deny from all
        </Directory>
        <Directory /var/www/$vhost_name>
                Order allow,deny
                allow from all
                AllowOverride FileInfo
                Options +FollowSymLinks
        </Directory>

        ErrorLog  "|/usr/bin/cronolog /atiba/apache/log/sites/$vhost_name/%Y/%Y-%m-%d-errors.log"
        CustomLog "|/usr/bin/cronolog /atiba/apache/log/sites/$vhost_name/%Y/%Y-%m-%d-access.log" common
        CustomLog "|/usr/bin/cronolog /atiba/apache/log/sites/all/%Y/%Y-%m-%d-access.log" commonvhost
</VirtualHost>
EOF

# set permissions for directory
sudo chown -R www-data:$ftpuser /var/www/$vhost_name
sudo chmod -R 775 /var/www/$vhost_name

# enable newly created virtualhost
sudo a2ensite $vhost_name

sudo /etc/init.d/apache2 restart

echo ""
echo ""
echo "====================================================="
echo -e "Setup Complete\n\nFTP:\nIP: 216.84.77.163\nusername: $ftpuser\npass: $ftppass\n\nMySQL:\nserver: localhost\ndatabase name:$dbname\nuser: $dbuser\npass: $dbpass"
